//
//  DeliveryDetailsController.swift
//  Delivery QQ
//
//  Created by Jeyakumaran Mayooresan on 9/19/18.
//  Copyright © 2018 Jeyakumaran Mayooresan. All rights reserved.
//

import UIKit
import MapKit

class DeliveryDetailsController: UIViewController, MKMapViewDelegate {

    var deliveryItem : DeliveryItem?
    
    var mainStackView : UIStackView = {
        let stackview =  UIStackView()
        stackview.translatesAutoresizingMaskIntoConstraints = false
        stackview.backgroundColor = UIColor.red
        stackview.distribution = .fillEqually
        stackview.alignment = .fill
        stackview.axis = .vertical
        stackview.spacing = 8
        return stackview
    }()
    
    var bottomStackView : UIStackView = {
        let stackview =  UIStackView()
        stackview.translatesAutoresizingMaskIntoConstraints = false
        stackview.backgroundColor = UIColor.red
        stackview.distribution = .fillProportionally
        stackview.alignment = .top
        stackview.axis = .horizontal
        stackview.spacing = 8
        return stackview
    }()
    
    var mapview : MKMapView = {
        let mapview = MKMapView()
        mapview.translatesAutoresizingMaskIntoConstraints = false
        return mapview
    }()
    
    
    let descriptionLabel : UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    let deliveryImageView : QQImageView = {
        let imageview = QQImageView()
        imageview.image = UIImage(named: "profile")
        imageview.translatesAutoresizingMaskIntoConstraints = false
        imageview.contentMode = .scaleAspectFill
        imageview.clipsToBounds = true
        return imageview
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapview.register(MKMarkerAnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
        setupLayout()
        setData()
    }
    
    func setupLayout(){
        view.backgroundColor = UIColor.white
        self.title = "Delivery Details"
        
        let guide = view.safeAreaLayoutGuide
        let margins = view.layoutMarginsGuide
        
        bottomStackView.addArrangedSubview(deliveryImageView)
        bottomStackView.addArrangedSubview(descriptionLabel)
        
        mainStackView.addArrangedSubview(mapview)
        mainStackView.addArrangedSubview(bottomStackView)
        
        view.addSubview(mainStackView)

        
        NSLayoutConstraint.activate([
            mainStackView.topAnchor.constraint(equalTo: guide.topAnchor, constant: 8),
            mainStackView.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
            mainStackView.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
            mainStackView.bottomAnchor.constraint(equalTo: guide.bottomAnchor , constant: 8)
            ])
    }
    
    func setData(){
        
        if let description = deliveryItem?.deliveryItemDescription {
            descriptionLabel.text = description
        }else{
            descriptionLabel.text = "No delivery description provided"
        }
        
        if let imageUrlString = deliveryItem?.deliveryItemImageUrl {
            let imageUrl = URL(string: imageUrlString)!
            deliveryImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "profile"), options: .refreshCached, completed: nil)
        }
        
        //set map releated data
        if let lat = deliveryItem?.deliveryLocation?.deliveryLatitude, let lon = deliveryItem?.deliveryLocation?.deliveryLongitute {
            let span:MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
            let location : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: lat, longitude: lon)
            let region : MKCoordinateRegion = MKCoordinateRegion(center: location, span: span)
            mapview.setRegion(region, animated: true)
            
            let title : String?
            if let annotationTitle = deliveryItem?.deliveryLocation?.deliveryAddress {
                title = annotationTitle
            }else{
                title = "No address"
            }
            
            // place a marker on the map
            let packageAnnotation : QQAnnotation = QQAnnotation(coordinates: location, title: title!, subtitle: descriptionLabel.text)
            mapview.addAnnotation(packageAnnotation)
            mapview.selectAnnotation(packageAnnotation, animated: true)
            
        }

    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let myannotation = mapView.dequeueReusableAnnotationView(withIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier) as? MKMarkerAnnotationView {
            myannotation.animatesWhenAdded = true
            myannotation.titleVisibility = .adaptive
            myannotation.subtitleVisibility = .adaptive
            myannotation.image = UIImage(named: "profile")
            
            return myannotation
        }
        
        return nil
    }
}

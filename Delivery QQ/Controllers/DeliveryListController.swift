//
//  ViewController.swift
//  Delivery QQ
//
//  Created by Jeyakumaran Mayooresan on 9/18/18.
//  Copyright © 2018 Jeyakumaran Mayooresan. All rights reserved.
//

import UIKit
import SVProgressHUD

class DeliveryListController: UITableViewController, NetworkErrorDelegate {
    
    

    private var deliveryItems : [DeliveryItem] = CoreDataStack.loadDeliveryItems()
    var currentOffset : Int = 0
    let deliveryListLimit : Int = 20
    var isCurrentlyLoadingitems : Bool = false
    
    let cellId = "Cell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //set the title in the navigtionitem
        self.title = "Things to Deliver"
        
        tableView.estimatedRowHeight = 66
        tableView.rowHeight = UITableView.automaticDimension
        currentOffset = deliveryItems.count 
        tableView.register(DeliveryCell.self, forCellReuseIdentifier: cellId)
        tableView.reloadData()
        
        //checking network availability before consumng the REST service.
        if Connectivity.isConnectedToInternet {
            loadDeliveryItemsFromService()
        }else{
            showNetworkErrorMessage()
        }
        
    }
    
    func didNetworkErrorOccured() {
        isCurrentlyLoadingitems = false
    }
    
    func showNetworkErrorMessage(){
        SVProgressHUD.showError(withStatus: "No ineternet connectivity. Touch and pull up the screen to try loading again.")
    }
    
    @objc func tryAgainButtonTapped(){
    
    }

    // MARK: -  TableView Related
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deliveryItems.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! DeliveryCell
        let deliveryItem : DeliveryItem = self.deliveryItems[indexPath.row]
        cell.setDeliveryItemForCell(deliveryItem: deliveryItem)
        cell.sizeToFit()
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        let deliveryItem : DeliveryItem = deliveryItems[indexPath.row]
        let vc = DeliveryDetailsController()
        vc.deliveryItem = deliveryItem
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        //view.backgroundColor = UIColor.red
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40
    }
    
    // MARK: - Auto loading new items over the network
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if tableView.contentOffset.y >= (tableView.contentSize.height - tableView.frame.size.height) {
            if self.isCurrentlyLoadingitems == false {
                if Connectivity.isConnectedToInternet {
                    loadDeliveryItemsFromService()
                }else{
                    showNetworkErrorMessage()
                }
            }
            
        }
    }
    
    @objc func loadDeliveryItemsFromService(){
        isCurrentlyLoadingitems = true
        
        let networkService = QQDeliveryItemsNetwork()
        networkService.delegate = self
        networkService.callGetDeliveryItemsService(offset: currentOffset, limit: deliveryListLimit, context: self, parameter: nil) { (deliveryItems, value) in
            self.deliveryItems = CoreDataStack.loadDeliveryItems()
            self.tableView.reloadData()
            self.isCurrentlyLoadingitems = false
            self.currentOffset += 20
        }

    }
    
}


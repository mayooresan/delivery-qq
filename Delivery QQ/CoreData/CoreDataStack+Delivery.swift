//
//  CoreDataStack+Delivery.swift
//  Delivery QQ
//
//  Created by Jeyakumaran Mayooresan on 9/19/18.
//  Copyright © 2018 Jeyakumaran Mayooresan. All rights reserved.
//

import Foundation
import CoreData

extension CoreDataStack {
    static func saveDeliveryItem (deliveryItem : DeliveryItem) {
        let delivery : Delivery = CoreDataStack.getEntity()
        let location : Location = CoreDataStack.getEntity()
        
        delivery.id = Int64(deliveryItem.id!)
        delivery.deliverydescription = deliveryItem.deliveryItemDescription!
        delivery.imageUrl = deliveryItem.deliveryItemImageUrl!
        
        
        location.lat = deliveryItem.deliveryLocation!.deliveryLatitude!
        location.lng = deliveryItem.deliveryLocation!.deliveryLongitute!
        location.address = deliveryItem.deliveryLocation!.deliveryAddress!
        
        delivery.location = location
        location.delivery = delivery
        
        CoreDataStack.saveContext()
        
    }
    
    static func saveDeliveryItems (deliveryItems : [DeliveryItem]) {
        for item in deliveryItems {
            CoreDataStack.saveDeliveryItem(deliveryItem: item)
        }
        
        do{
            let myArray = try CoreDataStack.managedObjectContext.fetch(Delivery.fetchRequest())
            print(myArray.count)
        }catch {
            print(error.localizedDescription)
        }
    }
    
    static func loadDeliveryItems()->[DeliveryItem]{
        var deliveryItems : [DeliveryItem] = []
        do{
            let deliveries : [Delivery] = try CoreDataStack.managedObjectContext.fetch(Delivery.fetchRequest())
            
            for delivery in deliveries {
                let deliveryLocation = DeliveryLocation(lat: (delivery.location?.lat)!, lng: (delivery.location?.lng)!, address: (delivery.location?.address)!)
                let deliveryItem = DeliveryItem(id: Int(delivery.id), description: delivery.deliverydescription!, imageUrl: delivery.imageUrl!, location: deliveryLocation)
                deliveryItems.append(deliveryItem)
            }
            
            print(deliveries.count)
        }catch {
            print(error.localizedDescription)
        }
        
        return deliveryItems
    }
}

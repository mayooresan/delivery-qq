//
//  Protocols+QQ.swift
//  Delivery QQ
//
//  Created by Jeyakumaran Mayooresan on 9/19/18.
//  Copyright © 2018 Jeyakumaran Mayooresan. All rights reserved.
//

import Foundation
protocol NetworkErrorDelegate{
    func didNetworkErrorOccured()
}

//
//  UIColor+QQSupport.swift
//  Delivery QQ
//
//  Created by Jeyakumaran Mayooresan on 9/19/18.
//  Copyright © 2018 Jeyakumaran Mayooresan. All rights reserved.
//

import UIKit
extension UIColor {
    static var deepOrange = UIColor(red: 241/255, green: 102/255, blue: 34/255, alpha: 1)
}

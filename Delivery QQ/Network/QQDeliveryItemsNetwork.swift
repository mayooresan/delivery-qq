//
//  QQDeliveryItemsNetwork.swift
//  Delivery QQ
//
//  Created by Jeyakumaran Mayooresan on 9/19/18.
//  Copyright © 2018 Jeyakumaran Mayooresan. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import SwiftyJSON
import SVProgressHUD

class QQDeliveryItemsNetwork {
    //delegate to notify network failure to viewcontroller
    var delegate : NetworkErrorDelegate?
    
    func callGetDeliveryItemsService(offset: Int, limit: Int, context :UIViewController, parameter : [String : Any]?, completion: @escaping ([DeliveryItem], String) -> ()){
        SVProgressHUD.show(withStatus: "Please wait...")
        QQNetworkService.shared().callService(path: "deliveries?offset=\(offset)&limit=\(limit)", parameters: nil, method: .get) { (Response, value) in

            let error = Response.result.error
            let req = Response.request
            let res = Response.response
            let json = Response.data
            SVProgressHUD.dismiss()
            
            if error != nil  {
                if req != nil && res != nil {
                    print(req!)
                    print(res!)
                }
                
                SVProgressHUD.showError(withStatus: "Unable to retrieve data. Please try again in a moment")
                self.delegate?.didNetworkErrorOccured()
                
            }else{
                do{
                    let parsedjson : JSON = try JSON(data : json!)
                    print(parsedjson.rawString()!)
                    if let deliveryItems = Mapper<DeliveryItem>().mapArray(JSONString: parsedjson.rawString()!){
                        //after the id 69, the service returns empty array. This condition is to tackle it.
                        if deliveryItems.count  == 0 {
                            self.delegate?.didNetworkErrorOccured()
                        }else{
                            CoreDataStack.saveDeliveryItems(deliveryItems: deliveryItems)
                            completion(deliveryItems, "done")
                        }
                        
                    }else{
                        self.delegate?.didNetworkErrorOccured()
                    }
                }catch{
                    SVProgressHUD.showError(withStatus: error.localizedDescription)
                    self.delegate?.didNetworkErrorOccured()
                }
            }
        }
    }
}

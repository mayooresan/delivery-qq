//
//  Connectivity.swift
//  Delivery QQ
//
//  Created by Jeyakumaran Mayooresan on 9/19/18.
//  Copyright © 2018 Jeyakumaran Mayooresan. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

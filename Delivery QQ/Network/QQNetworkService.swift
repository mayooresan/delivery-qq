//
//  QQNetworkService.swift
//  Delivery QQ
//
//  Created by Jeyakumaran Mayooresan on 9/19/18.
//  Copyright © 2018 Jeyakumaran Mayooresan. All rights reserved.
//

import Foundation
import Alamofire

class QQNetworkService {
    let baseURL: String

    
    //singleton object for the network service
    private static var sharedNetworkManager: QQNetworkService = {
        let networkService = QQNetworkService(baseURL: "https://mock-api-mobile.dev.lalamove.com/")
        return networkService
    }()
    
    private init(baseURL: String) {
        self.baseURL = baseURL
    }
    
    class func shared() -> QQNetworkService {
        return sharedNetworkManager
    }
    
    func getAlamoFireHeader() -> HTTPHeaders{
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        return headers
    }
    
    func callGetService(path: String, parameters : [String : Any]?, completion: @escaping (DataResponse<Any>, String) -> ()){
        self.callService(path: path, parameters: parameters, method: .get, completion: completion)
    }
    
    func callPostService(path: String, parameters : [String : Any]?, completion: @escaping (DataResponse<Any>, String) -> ()){
        self.callService(path: path, parameters: parameters, method: .post, completion: completion)
    }
    
    func callService(path: String, parameters : [String : Any]?, method: HTTPMethod, completion: @escaping (DataResponse<Any>, String) -> ()){
        Alamofire.request(self.baseURL+path, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: self.getAlamoFireHeader()).responseJSON { (data) in
            completion(data, "done")
        }
    }
}

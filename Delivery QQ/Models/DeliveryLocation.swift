//
//  DeliveryLocation.swift
//  Delivery QQ
//
//  Created by Jeyakumaran Mayooresan on 9/19/18.
//  Copyright © 2018 Jeyakumaran Mayooresan. All rights reserved.
//

import Foundation
import ObjectMapper

class DeliveryLocation : Mappable{
    var deliveryLatitude : Double?
    var deliveryLongitute : Double?
    var deliveryAddress : String?
    
    init(lat : Double, lng : Double, address : String) {
        self.deliveryLatitude = lat
        self.deliveryLongitute = lng
        self.deliveryAddress = address
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        deliveryLatitude <- map["lat"]
        deliveryLongitute <- map["lng"]
        deliveryAddress <- map["address"]
    }
}

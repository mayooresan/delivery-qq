//
//  DeliveryItem.swift
//  Delivery QQ
//
//  Created by Jeyakumaran Mayooresan on 9/19/18.
//  Copyright © 2018 Jeyakumaran Mayooresan. All rights reserved.
//

import Foundation
import ObjectMapper

class DeliveryItem: Mappable {
    var id : Int?
    var deliveryItemDescription : String?
    var deliveryItemImageUrl : String?
    var deliveryLocation : DeliveryLocation?
    
    init(id : Int, description : String, imageUrl : String, location : DeliveryLocation) {
        self.deliveryItemDescription = description
        self.deliveryItemImageUrl = imageUrl
        self.id = id
        self.deliveryLocation = location
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        deliveryItemDescription <- map["description"]
        deliveryItemImageUrl <- map["imageUrl"]
        deliveryLocation <- map["location"]
    }
    
}

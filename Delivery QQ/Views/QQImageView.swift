//
//  QQImageView.swift
//  Delivery QQ
//
//  Created by Jeyakumaran Mayooresan on 9/19/18.
//  Copyright © 2018 Jeyakumaran Mayooresan. All rights reserved.
//

import UIKit

class QQImageView: UIImageView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override open var intrinsicContentSize: CGSize {
        //matching the original size of the image to give more natual look
        return CGSize(width: 213, height: 120)
    }
}

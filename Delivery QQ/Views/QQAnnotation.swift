//
//  QQAnnotation.swift
//  Delivery QQ
//
//  Created by Jeyakumaran Mayooresan on 9/19/18.
//  Copyright © 2018 Jeyakumaran Mayooresan. All rights reserved.
//

import UIKit
import MapKit

class QQAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title : String?
    var subtitle: String?
    
    init(coordinates : CLLocationCoordinate2D, title : String?, subtitle : String?){
        self.coordinate = coordinates
        self.title = title
        self.subtitle = subtitle
        
        super.init()
    }
    
}

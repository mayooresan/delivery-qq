//
//  DeliveryCell.swift
//  Delivery QQ
//
//  Created by Jeyakumaran Mayooresan on 9/18/18.
//  Copyright © 2018 Jeyakumaran Mayooresan. All rights reserved.
//

import UIKit
import SDWebImage

class DeliveryCell: UITableViewCell {
    
    let descriptionLabel : UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    let addressLabel : UILabel = {
        let label = UILabel()
        label.text = ""
        label.textColor = UIColor.gray
        label.font = UIFont.boldSystemFont(ofSize: 11)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    let deliveryImageView : UIImageView = {
        let imageview = UIImageView()
        imageview.image = UIImage(named: "profile")
        imageview.translatesAutoresizingMaskIntoConstraints = false
        imageview.contentMode = .scaleAspectFill
        imageview.clipsToBounds = true
        return imageview
    }()
    
    var deliveryItem : DeliveryItem? {
        didSet{
            if let adddress = deliveryItem?.deliveryLocation?.deliveryAddress {
                addressLabel.text = adddress
            }else {
                addressLabel.text = "Sorry no address found"
            }
            
            if let description = deliveryItem?.deliveryItemDescription {
                descriptionLabel.text =  description
            }else{
                descriptionLabel.text = "No delivery description provided"
            }
            
            if let imageUrlString = deliveryItem?.deliveryItemImageUrl {
                let imageUrl = URL(string: imageUrlString)!
                deliveryImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "profile"), options: .refreshCached, completed: nil)
            }
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        deliveryImageView.image = UIImage(named: "profile")
        setupLayout()
    }
    
    func setDeliveryItemForCell(deliveryItem : DeliveryItem){
        self.deliveryItem = deliveryItem
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupLayout(){
        backgroundColor = UIColor.white
        
        addSubview(deliveryImageView)
        addSubview(descriptionLabel)
        addSubview(addressLabel)
        

        deliveryImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        deliveryImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        deliveryImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        deliveryImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        deliveryImageView.layer.cornerRadius = 25
        
        descriptionLabel.leftAnchor.constraint(equalTo: deliveryImageView.rightAnchor, constant: 8).isActive = true
        descriptionLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 8).isActive = true
        descriptionLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 8).isActive = true
        descriptionLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        addressLabel.leftAnchor.constraint(equalTo: deliveryImageView.rightAnchor, constant: 8).isActive = true
        addressLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 8).isActive = true
        addressLabel.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor).isActive = true
        addressLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
    }

}

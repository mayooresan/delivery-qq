# Delivery QQ

This is a sample app to show how we can consume rest api and show list of delivery items available for a biker. Further, biker can click an item and view more details.

## Getting the Project Ready

Please make sure you have Cocoapods installed on  your mac machine. Navigate to the root of the project and type `pod install` to install the dependancies to run the project. Once the pods are installed open the file `Delivery QQ.xcworkspace` and run the project.

## Libraries used in this project

* SwiftyJSON : Easily pass JSON response
* Alamofire : Easy networking with iOS
* ObjectMapper : Map JSON objects with Swift entities
* SVProgressHUD : Show loader to users. This will help them to understand that some background task is being executed.
* SDWebImage : Cach and load images over the network and show it in UIImageView.

## Orientation
The app is deisnged to work in both portrait and landscape mode. UIStackView and AutoLayout are used to handle the different orientations. 

## Screenshots

### Screenshot of the home screen
![home](https://bytebucket.org/mayooresan/delivery-qq/raw/fa9f7f463f5862dbbb7f345e1f04a6f7eee1cad0/screenshots/ss1.png "home screen")

### Detail view
![detail](https://bytebucket.org/mayooresan/delivery-qq/raw/0f29d0a4aa09278a26323f39ab5e8dc1ea718d85/screenshots/ss2.png "detail screen")
